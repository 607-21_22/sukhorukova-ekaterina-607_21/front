import React, { useState } from 'react'
import axios from 'axios'
import { API_BASE } from '../main'
import Button from '../components/Button'
import Loader from '../components/Loader/Loader'
import { IClient } from './Client'
import { IProduct } from './Product'


interface IOrder {
    id: number,
    status: boolean,
    client: IClient,
    product: IProduct
}

const Order: React.FC = () => {

    const [order, setOrder] = useState<IOrder | undefined>()
    const [clientFio, setClientFio] = useState<string>('')
    const [productName, setProductName] = useState<string>('')
    const [res, setRes] = useState<string[]>([''])
    const [isLoad, setIsLoad] = useState<boolean>(false)
    const [orderNumber, setOrderNumber] = useState<number>(0)
    const [clientId, setclientId] = useState<number>(0)

    // Очистка инпутов
    const clearInput = () => {
        setClientFio('')
        setProductName('')
        setOrderNumber(0)
        setclientId(0)
    }

    // Создать заказ
    const createOrder = async () => {
        setIsLoad(true)
        setRes([''])
        setOrder(undefined)
        await axios.get(`${API_BASE}/clients/fio/${clientFio}`)
            .then(async (result) => {
                if (result.data == 'Nothing was found') setRes(["Клиента с таким ФИО нет в БД", ...res])
                else {
                    setclientId(result.data.id)
                    await axios.get(`${API_BASE}/products/title/${productName}`)
                        .then(async (result) => {
                            if (result.data == 'Nothing was found') setRes(["Такого товара нет в БД", ...res])
                            else {
                                await axios.post(`${API_BASE}/order/create`, {
                                    client: clientId,
                                    product: result.data.id
                                })
                                    .then(r => {
                                        setRes([r.statusText, ...res])
                                    })
                                    .catch((err) => {
                                        setRes([err.response.data.message, ...res])
                                    })
                            }
                        })
                }
            })
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Получить заказ по его номеру
    const getOrderById = async () => {
        setOrder(undefined)
        setIsLoad(true)
        setOrderNumber(0)
        await axios.get(`${API_BASE}/order/${orderNumber}`)
            .then(r => {
                console.log(r)
                setOrder(undefined)
                r.data === "Nothing was found" ? setRes([r.data, ...res]) : setOrder(r.data)
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Получить заказ по id клиента
    const getOrderByClientId = async () => {
        setIsLoad(true)
        setOrderNumber(0)
        await axios.get(`${API_BASE}/order/getall/${clientId}`)
            .then(r => {
                (r.data === "Nothing was found" || r.data === "Клиента с таким id нет в базе") ? setRes([r.data, ...res]) : setOrder(r.data)
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => setIsLoad(false))
    }

    // Очистить вывод
    const clearWindow = () => {
        setRes([''])
        setOrder(undefined)
    }

    return (
        <div className="flex flex-col gap-10 p-5 text-center w-full bg-gray" id="order">
            <h2 className="text-lg font-bold">Добавить заказ</h2>
            <div className="border-b p-5">
                <div className=" flex items-center justify-center gap-20">
                    <label className="flex flex-col items-start gap-2">
                        Введите ФИО клиента
                        <input type="text" value={clientFio} className="border p-1 rounded-md w-64" placeholder="Введите ФИО" onChange={(e) => setClientFio(e.target.value)} />
                    </label>
                    <label className="flex flex-col items-start gap-2">
                        Введите название продукта
                        <input type="text" value={productName} className="border p-1 rounded-md w-64" placeholder="Введите адрес" onChange={(e) => setProductName(e.target.value)} />
                    </label>
                </div>
                <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={createOrder} text="Добавить" />
            </div>
            <div className="border-b p-7 flex items-center justify-center gap-20">
                <div className="">
                    <h2 className="pb-10 text-lg font-bold">Получить заказ по его номеру</h2>
                    <div className=" flex items-center justify-center gap-20">
                        <label className="">
                            <input type="text" value={orderNumber} className="border p-1 rounded-md w-64" placeholder="Введите номер заказа" onChange={(e) => setOrderNumber(Number(e.target.value))} />
                        </label>
                    </div>
                    <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={getOrderById} text="Получить" />
                </div>
                <div className="">
                    <h2 className="pb-10 text-lg font-bold">Получить заказы по id клиента</h2>
                    <div className=" flex items-center justify-center gap-20">
                        <label className="">
                            <input type="text" value={clientId} className="border p-1 rounded-md w-64" placeholder="Введите номер заказа" onChange={(e) => setclientId(Number(e.target.value))} />
                        </label>
                    </div>
                    <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={getOrderByClientId} text="Получить" />
                </div>
            </div>
            <h2 className="font-bold text-lg pb-3">Результат запроса</h2>
            <div className="w-full bg-white h-64 text-left overflow-y-scroll">
                <div className="p-5 flex flex-col gap-1">
                    {isLoad ? <Loader myStyle={'mt-20'} /> : (order ?
                        (Array.isArray(order) ?
                            order.map(o =>
                                <div key={o.id} className="flex items-center gap-5 justify-center border-b py-2">
                                    <p className='flex flex-col items-center justify-center gap-1'>id заказа <span>{o.id}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Клиент <span>{o.client.FIO}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Продукты <span>{o.product.title}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Статус <span>{o.status}</span></p>
                                </div>
                            )
                            :
                            <div className="flex items-center gap-5 justify-center">
                                <p className='flex flex-col items-center justify-center gap-1'>id заказа <span>{order.id}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Клиент <span>{order.client.FIO}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Продукты <span>{order.product.title}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Статус <span>{order.status}</span></p>
                            </div>)
                        : (res && res.map((r, index) => <div key={index}>{r}</div>)))}
                </div>
            </div>
            <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md' func={clearWindow} text='Очистить окно вывода' />
        </div>
    )
}

export default Order