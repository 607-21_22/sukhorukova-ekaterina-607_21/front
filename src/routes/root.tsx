import React from 'react';
import Client from './Client';
import Header from '../components/Header'
import { Outlet } from "react-router-dom";

export default function Root() {
    return (
        <>
            <div id='header' className="box-border">
                <Header />
                <div id="detail">
                    <Outlet />
                </div>
            </div >
        </>
    )
}