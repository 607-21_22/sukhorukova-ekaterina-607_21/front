import React, { useState } from 'react'
import axios from 'axios'
import { API_BASE } from '../main'
import Button from '../components/Button'
import Loader from '../components/Loader/Loader'
import { Link } from 'react-router-dom'


export interface IClient {
    id: number,
    FIO: string,
    phone: string,
    adres: string
}

const Client = () => {

    const [client, setClient] = useState<IClient | undefined>()
    const [fio, setFio] = useState<string>('')
    const [adres, setAdres] = useState<string>('')
    const [phone, setPhone] = useState<string>('')
    const [res, setRes] = useState<string[]>([''])
    const [isLoad, setIsLoad] = useState<boolean>(false)
    const [fioForSeacrh, setFioForSeacrh] = useState<string>('')

    // Очистка инпутов
    const clearInput = () => {
        setFio('')
        setAdres('')
        setPhone('')
        setFioForSeacrh('')
    }

    // Создать клиента
    const createClient = async () => {
        setIsLoad(true)
        setRes([''])
        setClient(undefined)
        await axios.post(`${API_BASE}/clients/create`, {
            FIO: fio,
            adres: adres,
            phone: phone
        })
            .then(r => {
                setRes([r.statusText, ...res])
            })
            .catch((err) => setRes([err.response.data.message, ...res]))
            .finally(() => {
                clearInput()
                setIsLoad(false)
            })
    }

    // Получить клиента по ФИО
    const getClientByFio = async () => {
        setIsLoad(true)
        setFioForSeacrh('')
        await axios.get(`${API_BASE}/clients/fio/${fioForSeacrh}`)
            .then(r => {
                setClient(r.data)
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => setIsLoad(false))
    }

    // Очистить вывод
    const clearWindow = () => {
        setRes([''])
        setClient(undefined)
    }

    return (
        <div className="flex flex-col gap-10 p-5 text-center w-full bg-gray" id="client">
            <h2 className="pb-10 text-lg font-bold">Добавить клиента</h2>
            <div className="border-b p-5">
                <div className=" flex items-center justify-center gap-20">
                    <label className="flex flex-col items-start gap-2">
                        Введите ФИО
                        <input type="text" value={fio} className="border p-1 rounded-md w-64" placeholder="Введите ФИО" onChange={(e) => setFio(e.target.value)} />
                    </label>
                    <label className="flex flex-col items-start gap-2">
                        Введите адрес
                        <input type="text" value={adres} className="border p-1 rounded-md w-64" placeholder="Введите адрес" onChange={(e) => setAdres(e.target.value)} />
                    </label>
                    <label className="flex flex-col items-start justify-start gap-2">
                        Введите номер телефона
                        <input type="text" value={phone} className="border p-1 rounded-md w-64" placeholder="Введите номер телефона" onChange={(e) => setPhone(e.target.value)} />
                    </label>
                </div>
                <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={createClient} text="Добавить" />
            </div>
            <div className="border-b p-5">
                <h2 className="pb-10 text-lg font-bold">Получить клиента по ФИО</h2>
                <div className=" flex items-center justify-center gap-20">
                    <label className="">
                        <input type="text" value={fioForSeacrh} className="border p-1 rounded-md w-64" placeholder="Введите ФИО" onChange={(e) => setFioForSeacrh(e.target.value)} />
                    </label>
                </div>
                <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={getClientByFio} text="Получить" />
            </div>
            <h2 className="font-bold text-lg pb-3">Результат запроса</h2>
            <div className="w-full bg-white h-64 text-left overflow-y-scroll">
                <div className="p-5 flex flex-col gap-1">
                    {isLoad ? <Loader myStyle={'mt-20'} /> : (client ?
                        <div className="flex items-center gap-5 justify-center">
                            <p className='flex flex-col items-center justify-center gap-1'>id клиента <span>{client.id}</span></p>
                            <p className='flex flex-col items-center justify-center gap-1'>ФИО клиента <span>{client.FIO}</span></p>
                            <p className='flex flex-col items-center justify-center gap-1'>Адрес клиента <span>{client.adres}</span></p>
                            <p className='flex flex-col items-center justify-center gap-1'>Телефон клиента <span>{client.phone}</span></p>
                        </div>
                        : (res && res.map((r, index) => <div key={index}>{r}</div>)))}
                </div>
            </div>
            <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md' func={clearWindow} text='Очистить окно вывода' />
        </div>
    )
}

export default Client