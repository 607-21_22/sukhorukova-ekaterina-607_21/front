import React, { useState } from 'react'
import axios, { HttpStatusCode } from 'axios'
import { API_BASE } from '../main'
import Button from '../components/Button'
import Loader from '../components/Loader/Loader'


export interface IProduct {
    id: number,
    title: string,
    category: string,
    price: number,
    count: number,
    order: number
}


const Product = () => {

    const [product, setProduct] = useState<IProduct | undefined | IProduct[]>()
    const [title, setTitle] = useState<string>('')
    const [category, setCategory] = useState<string>('')
    const [price, setPrice] = useState<number>(0)
    const [count, setCount] = useState<number>(0)
    const [res, setRes] = useState<string[]>([''])
    const [isLoad, setIsLoad] = useState<boolean>(false)
    const [idForDelete, setIdForDelete] = useState<number>(0)
    const [titleForSearch, setTitleForSearch] = useState<string>('')

    // Очистка инпутов
    const clearInput = () => {
        setTitle('')
        setCategory('')
        setPrice(0)
        setCount(0)
        setIdForDelete(0)
        setTitleForSearch('')
    }

    // Создать продукт
    const createProduct = async () => {
        setIsLoad(true)
        setRes([''])
        await axios.post(`${API_BASE}/products/create`, {
            title: title,
            category: category,
            price: price,
            count: count
        })
            .then(r => {
                setRes([r.statusText, ...res])
            })
            .catch((err) => setRes([err.response.data.message, ...res]))
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Получить продукт по названию
    const getProductByTitle = async () => {
        setProduct(undefined)
        setIsLoad(true)
        setTitle('')
        await axios.get(`${API_BASE}/products/title/${titleForSearch}`)
            .then(r => {
                r.data === "Nothing was found" ? setRes([r.data, ...res]) : setProduct(r.data)
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Получить продукт по кол-во на складе больше параметра
    const getProductByCount = async () => {
        setProduct(undefined)
        setIsLoad(true)
        await axios.get(`${API_BASE}/products/count/${count}`)
            .then(r => {
                r.data === "Nothing was found" ? setRes([r.data, ...res]) : setProduct(r.data)
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Удалить продукт
    const deleteProduct = async () => {
        setIsLoad(true)
        setProduct(undefined)
        await axios.delete(`${API_BASE}/products/delete/${idForDelete}`)
            .then(r => {
                console.log(r);
                setRes([r.data, ...res])
            })
            .catch(err => {
                setRes([err.message, ...res])
            })
            .finally(() => {
                setIsLoad(false)
                clearInput()
            })
    }

    // Очистить вывод
    const clearWindow = () => {
        setRes([''])
        setProduct(undefined)
    }

    return (
        <div className="flex flex-col gap-10 p-5 text-center w-full bg-gray" id="product">
            <h2 className="text-lg font-bold">Добавить продукт на склад</h2>
            <div className="border-b p-5">
                <div className="grid grid-cols-2 ml-[16%]">
                    <label className="flex flex-col items-start gap-2">
                        Введите название товара
                        <input type="text" value={title} className="border p-1 rounded-md w-64" placeholder="название" onChange={(e) => setTitle(e.target.value)} />
                    </label>
                    <label className="flex flex-col items-start gap-2">
                        Введите категорию товара
                        <input type="text" value={category} className="border p-1 rounded-md w-64" placeholder="категория" onChange={(e) => setCategory(e.target.value)} />
                    </label>
                    <label className="flex flex-col items-start justify-start gap-2">
                        Введите цену товара
                        <input type="text" value={price} className="border p-1 rounded-md w-64" placeholder="цена" onChange={(e) => setPrice(Number(e.target.value))} />
                    </label>
                    <label className="flex flex-col items-start justify-start gap-2">
                        Введите кол-во товара на складе
                        <input type="text" value={count} className="border p-1 rounded-md w-64" placeholder="кол-во" onChange={(e) => setCount(Number(e.target.value))} />
                    </label>
                </div>
                <Button stylesContainer='mt-10' stylesButton='py-1 px-2 border rounded-md' func={createProduct} text="Добавить" />
            </div>
            <h2 className=" text-lg font-bold">Получить продукт</h2>
            <div className="border-b p-7 flex items-center justify-center gap-20">
                <div className="flex flex-col gap-7">
                    <h2 className="text-lg font-semibold">По названию</h2>
                    <div className=" flex items-center justify-center gap-20">
                        <label className="">
                            <input type="text" value={titleForSearch} className="border p-1 rounded-md w-64" placeholder="Введите название" onChange={(e) => setTitleForSearch(e.target.value)} />
                        </label>
                    </div>
                    <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md text-sm ' func={getProductByTitle} text="Получить" />
                </div>
                <div className="flex flex-col gap-7">
                    <h2 className="text-lg font-semibold">По количеству (больше чем)</h2>
                    <div className=" flex items-center justify-center gap-20">
                        <label className="">
                            <input type="text" value={count} className="border p-1 rounded-md w-64" placeholder="Введите число" onChange={(e) => setCount(Number(e.target.value))} />
                        </label>
                    </div>
                    <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md text-sm' func={getProductByCount} text="Получить" />
                </div>
                <div className="flex flex-col gap-7">
                    <h2 className="text-lg font-semibold">Удалить продукт по id</h2>
                    <div className=" flex items-center justify-center gap-20">
                        <label className="">
                            <input type="text" value={idForDelete} className="border p-1 rounded-md w-64" placeholder="Введите id" onChange={(e) => setIdForDelete(Number(e.target.value))} />
                        </label>
                    </div>
                    <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md text-sm ' func={deleteProduct} text="Удалить" />
                </div>
            </div>
            <h2 className="font-bold text-lg pb-3">Результат запроса</h2>
            <div className="w-full bg-white h-64 text-left overflow-y-scroll">
                <div className="p-5 flex flex-col gap-1">
                    {isLoad ? <Loader myStyle={'mt-20'} /> : (product ?
                        (Array.isArray(product) ?
                            product.map(p =>
                                <div className="flex items-center gap-5 justify-center border-b py-2">
                                    <p className='flex flex-col items-center justify-center gap-1'>id продукта <span>{p.id}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Название <span>{p.title}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Категория <span>{p.category}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Цена <span>{p.price}</span></p>
                                    <p className='flex flex-col items-center justify-center gap-1'>Кол-во на складе <span>{p.count}</span></p>
                                </div>
                            )
                            :
                            <div className="flex items-center gap-5 justify-center">
                                <p className='flex flex-col items-center justify-center gap-1'>id продукта <span>{product.id}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Название <span>{product.title}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Категория <span>{product.category}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Цена <span>{product.price}</span></p>
                                <p className='flex flex-col items-center justify-center gap-1'>Кол-во на складе <span>{product.count}</span></p>
                            </div>)
                        : (res && res.map((r, index) => <div key={index}>{r}</div>)))}
                </div>
            </div>
            <Button stylesContainer='' stylesButton='py-1 px-2 border rounded-md' func={clearWindow} text='Очистить окно вывода' />
        </div>
    )
}

export default Product