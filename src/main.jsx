import React from 'react'
import ReactDOM from 'react-dom/client'
import Root from "./routes/root.tsx";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './index.css'
import ErrorPage from './errorPage.tsx'
import Order from './routes/Order.tsx';
import Product from './routes/Product.tsx';
import Client from './routes/Client.tsx';

export const API_BASE = 'http://localhost:4000'

const router = createBrowserRouter([
  {
    path: "/",
    element:
      <div className='py-7 px-16'>
        <Root />
      </div>,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "order",
        element:
          <div className=" py-12 2xl:max-w-[1300px] xl:max-w-[1100px] m-auto lg:max-w-[900px] md:max-w-[650px] sm:max-w-[500px] xs:max-w-[300px]">
            <Order />
          </div>
      },
      {
        path: "product",
        element:
          <div className=" py-12 2xl:max-w-[1300px] xl:max-w-[1100px] m-auto lg:max-w-[900px] md:max-w-[650px] sm:max-w-[500px] xs:max-w-[300px]">
            <Product />
          </div>
      },
      {
        path: "client",
        element:
          <div className=" py-12 2xl:max-w-[1300px] xl:max-w-[1100px] m-auto lg:max-w-[900px] md:max-w-[650px] sm:max-w-[500px] xs:max-w-[300px]">
            <Client />
          </div>
      }
    ]
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
