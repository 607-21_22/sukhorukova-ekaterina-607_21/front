import { useRouteError } from "react-router-dom"
import React from "react"

const ErrorPage = () => {

    const error: any = useRouteError()

    return (
        <div id="error-page" className=" h-screen flex items-center justify-center text-white">
            <div className="flex flex-col gap-5 items-center justify-center w-96 h-72 bg-zinc-700 rounded-2xl">
                <h1 className="font-bold">Oops!</h1>
                <p>Sorry, an unexpected error has occurred.</p>
                <p>
                    <i className="font-['Raleway']">{error.statusText || error.message}</i>
                </p>
            </div>
        </div>
    );
}

export default ErrorPage