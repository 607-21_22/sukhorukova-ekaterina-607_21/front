import React from 'react'

interface Props {
    func: () => void,
    text: string,
    stylesContainer: string,
    stylesButton: string
}


const Button: React.FC<Props> = ({
    func,
    text,
    stylesContainer,
    stylesButton
}) => {

    return (
        <div className={stylesContainer}>
            <button className={stylesButton} onClick={func}>{text}</button>
        </div>
    )
}

export default Button