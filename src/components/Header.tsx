import React from 'react'
import { Link } from 'react-router-dom'


const Header = () => {

    const links = [
        {
            route: '/order',
            title: 'Заказы'
        },
        {
            route: '/product',
            title: 'Товары'
        },
        {
            route: '/client',
            title: 'Клиенты'
        }
    ]

    return (
        <div className="flex items-center justify-between">
            <Link to={'/'} className="font-bold text-slate-800">Приложение склад</Link>
            <nav className='flex items-center justify-center gap-10'>
                {links.map(link => <Link key={link.title} className='hover:text-gray' to={link.route}>
                    {link.title}
                </Link>)}
            </nav>
        </div>
    )
}

export default Header