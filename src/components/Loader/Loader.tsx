import React from 'react'
import styles from './loader.module.css'

const Loader: React.FC<{
    myStyle: string
}> = ({
    myStyle
}) => {
        return (
            <div className={styles.spinner_container + ' ' + myStyle}>
                <div className={styles.loading_spinner}></div>
            </div >
        )
    }

export default Loader 